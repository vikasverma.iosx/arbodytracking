//
//  ViewController.swift
//  BodyTracking
//
//  Created by Vikas Verma on 19/03/21.
//

import UIKit
import RealityKit
import ARKit
import Combine
import ReplayKit

//import Photos

class ViewController: UIViewController,  ARSessionDelegate {

   
    @IBOutlet weak var sceneView: ARView!
    let characterAnchor = AnchorEntity()
    var character: BodyTrackedEntity?
    let characterOffset: SIMD3<Float> = [-1.0, 0, 0] // Offset the character by one meter to the left
    
    var printoutText: String = ""
    
    var bodySkeleton: BodySkeleton?
    var bodySkeletonAnchor = AnchorEntity()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.session.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        let configuration = ARBodyTrackingConfiguration()
        sceneView.session.run(configuration)
        sceneView.scene.addAnchor(bodySkeletonAnchor)
        
        
        var cancellable: AnyCancellable? = nil
        cancellable = Entity.loadBodyTrackedAsync(named: "character/robot.usdz").sink(
            receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    print("Error: Unable to load model: \(error.localizedDescription)")
                }
                cancellable?.cancel()
        }, receiveValue: { (character: Entity) in
            if let character = character as? BodyTrackedEntity {
                // Scale the character to human size
                character.scale = [1.0, 1.0, 1.0]
                self.character = character
                cancellable?.cancel()
            } else {
                print("Error: Unable to load model as BodyTrackedEntity")
            }
        })
        
    }
    
    

    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        DispatchQueue.main.async {
            for anchor in anchors {
                guard let bodyAnchor = anchor as? ARBodyAnchor else { continue }
                
//                // Update the position of the character anchor's position.
//                let bodyPosition = simd_make_float3(bodyAnchor.transform.columns.3)
//                self.characterAnchor.position = bodyPosition + self.characterOffset
//                // Also copy over the rotation of the body anchor, because the skeleton's pose
//                // in the world is relative to the body anchor's rotation.
//                self.characterAnchor.orientation = Transform(matrix: bodyAnchor.transform).rotation
//
//                if let character = self.character, character.parent == nil {
//                    // Attach the character to its anchor as soon as
//                    // 1. the body anchor was detected and
//                    // 2. the character was loaded.
//                    self.characterAnchor.addChild(character)
//                }

//                let skeleton = bodyAnchor.skeleton
//                let rootJointTransform = skeleton.modelTransform(for: .root)!
//                let rootjontposition  = simd_make_float3(rootJointTransform.columns.3)
//                print("Root: \(rootjontposition)")
//
//
//                let leftHandTransform = skeleton.modelTransform(for: .leftHand)!
//                let leftHandOffset = simd_make_float3(leftHandTransform.columns.3)
//                let leftHandPostion = rootjontposition + leftHandOffset
//                print("leftHand: \(leftHandPostion)")
                
                if let skeleton = self.bodySkeleton {
                    skeleton.update(with: bodyAnchor)
                    
                } else {
                    let skeleton  = BodySkeleton(for: bodyAnchor)
                    self.bodySkeleton = skeleton
                    self.bodySkeletonAnchor.addChild(skeleton)
                }
            }
        }
    }
    
    
    func writeLowerBodyAnchors(anchor: ARBodyAnchor) {
        // adding to a string
        // goal is to save the knee transform(s) to start
        let rightFootTransform = anchor.skeleton.localTransform(for: .rightFoot)
        let leftFootTransform = anchor.skeleton.localTransform(for: .leftFoot)

        printoutText += "\n local transform for right foot: \(rightFootTransform!)"
        printoutText += "\n local transform for left foot: \(leftFootTransform!)"
        printoutText += "\n local transform for root: \(anchor.skeleton.localTransform(for: .root)!)"
        
        // goal is to annotate the left and right feet with custom views... very taxing
        
        /*
        let leftFootAnchor = AnchorEntity()
        arView.scene.addAnchor(leftFootAnchor)
        let sphere = MeshResource.generateSphere(radius: 0.5)
        let entity = ModelEntity(mesh: sphere)
        if let matrix = rightFootTransform {
   
        }
         */
    }

}

class BodySkeleton: Entity {

    var joints: [String: Entity] = [:]  //
  
    required init(for bodyAnchor: ARBodyAnchor ) {
        super.init()
        // create entity for each joint in skeleton

        for jointName in ARSkeletonDefinition.defaultBody3D.jointNames {
            // default values for joint appearance
            var jointRadius: Float = 0.03
            var jointColor: UIColor  = .green
            
            switch jointName {
            
            case "neck_1_joint", "neck_2_joint", "neck_3_joint,neck_4_joint", "head_joint" ,"right_shoulder_1_joint", "left_shoulder_1_joint" :
                jointRadius *= 0.5
             
            case "jaw_joint", "chin_joint", "nose_joint", "right_eye_joint", "right_eyeUpperLid_joint","right_eyeLowerLid_joint","right_eyeball_joint","left_eye_joint", "left_eyeUpperLid_joint", "left_eyeLowerLid_joint", "left_eyeball_joint" :
                jointRadius *= 0.2
                jointColor = .yellow
                
            case _ where jointName.hasPrefix("spine_"):
                jointRadius *= 0.75
                
            case "right_hand_joint", "left_hand_joint":
                jointRadius *= 1
                jointColor = .green
                
            case _ where jointName.hasPrefix("left_hand") || jointName.hasPrefix("right_hand"):
                
                jointRadius *= 0.25
                jointColor = .yellow
                
            case _ where jointName.hasPrefix("right_toes") || jointName.hasPrefix("left_toes"):
                jointRadius *= 0.5
                jointColor = .yellow
                
            default:
                jointRadius *= 0.5
                jointColor = .green
                
                
            }


            let jointEntity = makeJoint (radius: jointRadius, color: jointColor )
            joints[jointName] = jointEntity
            self.addChild(jointEntity)

        }

        self.update(with: bodyAnchor)

    }
 
    required init() {
        fatalError("init() has not been implemented")
    }

    func makeJoint(radius: Float, color: UIColor) -> Entity {
        let mesh =  MeshResource.generateSphere(radius: radius)
        let meterial = SimpleMaterial(color: color, roughness: 0.8, isMetallic: false)
        let modelEntity  =  ModelEntity(mesh: mesh, materials: [meterial] )
        return modelEntity

    }

    func update (with bodyAnchor: ARBodyAnchor ){
        let rootPostions = simd_make_float3(bodyAnchor.transform.columns.3)
        for jointName in ARSkeletonDefinition.defaultBody3D.jointNames {
            if let jointEntity  = joints[jointName], let jointTransform  = bodyAnchor.skeleton.modelTransform(for: ARSkeleton.JointName(rawValue: jointName)) {
                let jointOffset = simd_make_float3(jointTransform.columns.3)
                jointEntity.position = rootPostions + jointOffset
                jointEntity.orientation = Transform(matrix: jointTransform).rotation

            }
        }
    }
}

